import {
  ISetTodoFilterAction
} from '../actions/todosActions'
import Constants from '../constants'

const filterReducer: Redux.Reducer<ITodoFilter> = (
  state: ITodoFilter = 'All',
  action: ISetTodoFilterAction
): ITodoFilter => {

  switch (action.type) {

    case Constants.ActionType.Todos.SET_TODO_FILTER:
      return (action as ISetTodoFilterAction).filter

    default:
      return state

  }

}

export default filterReducer
