import Todo from './Todo'

export interface ITodoListProps {
  todos: ITodoList
  toggleTodoStatus: (id: number) => void
  deleteTodo: (id: number) => void
}

const TodoList = (props: Readonly<ITodoListProps>) => {

  const { todos, toggleTodoStatus, deleteTodo } = props

  return (
    <div className="list-group">
      {
        todos.map(todo =>
          <Todo todo={todo} key={todo.id} toggleTodoStatus={toggleTodoStatus} deleteTodo={deleteTodo} />
        )
      }
    </div>
  )

}

export default TodoList
