import Constants from '../constants'
import * as Redux from 'redux'

export interface IDeleteAllTodosAction extends Redux.Action {
}

export const deleteAllTodosAction = (): IDeleteAllTodosAction => {
  return {
    type: Constants.ActionType.Todos.DELETE_ALL_TODOS
  }
}

export interface IAddTodoAction extends Redux.Action {
  text: string
}

export const addTodoAction = (text: string): IAddTodoAction => {
  return {
    type: Constants.ActionType.Todos.ADD_TODO,
    text
  }
}

export interface IToggleTodoStatusAction extends Redux.Action {
  id: number
}

export const toggleTodoStatusAction = (id: number): IToggleTodoStatusAction => {
  return {
    type: Constants.ActionType.Todos.TOGGLE_TODO_STATUS,
    id
  }
}

export interface IDeleteTodoAction extends Redux.Action {
  id: number
}

export const deleteTodoAction = (id: number): IDeleteTodoAction => {
  return {
    type: Constants.ActionType.Todos.DELETE_TODO,
    id
  }
}

export interface ISetTodoFilterAction extends Redux.Action {
  filter: ITodoFilter
}

export const setTodoFilterAction = (filter: ITodoFilter): ISetTodoFilterAction => {
  return {
    type: Constants.ActionType.Todos.SET_TODO_FILTER,
    filter
  }
}
