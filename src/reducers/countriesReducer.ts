import { IGetAllCountriesResultAction } from '../actions/countriesActions'
import Constants from '../constants'
import * as Immutable from 'immutable'

const countriesReducer: Redux.Reducer<ICountryList> = (
  state: ICountryList = Immutable.List<ICountry>(),
  action: Redux.Action
): ICountryList => {

  switch (action.type) {

    case Constants.ActionType.Countries.GET_ALL_COUNTRIES_RESULT:
      return state.push(...(action as IGetAllCountriesResultAction).countries)

    default:
      return state

  }
}

export default countriesReducer
