import {
  IAddTodoAction,
  IDeleteTodoAction,
  IToggleTodoStatusAction
} from '../actions/todosActions'
import Constants from '../constants'
import * as Immutable from 'immutable'

const todosReducer: Redux.Reducer<ITodoList> = (
  state: ITodoList = Immutable.List<ITodo>(),
  action: Redux.Action
): ITodoList => {

  switch (action.type) {

    case Constants.ActionType.Todos.ADD_TODO:
      return state.push({
        id: new Date().getTime(),
        text: (action as IAddTodoAction).text,
        completed: false
      })

    case Constants.ActionType.Todos.TOGGLE_TODO_STATUS:
      return state.update(
        state.findIndex(todo => todo.id === (action as IToggleTodoStatusAction).id),
        todo => {
          return {
            id: todo.id,
            text: todo.text,
            completed: todo.id === (action as IToggleTodoStatusAction).id ? !todo.completed : todo.completed
          }
        }
      )

    case Constants.ActionType.Todos.DELETE_TODO:
      return state.filter(todo => {
        return todo.id !== (action as IDeleteTodoAction).id
      })

    case Constants.ActionType.Todos.DELETE_ALL_TODOS:
      return state.clear()

    default:
      return state

  }
}

export default todosReducer
