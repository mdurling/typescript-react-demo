import countriesSaga from './countriesSaga'
import { Saga } from 'redux-saga'
import { Effect } from 'redux-saga/effects'

const rootSaga: Saga = function* (): Iterable<Effect> {
  yield [
    countriesSaga()
  ]
}

export default rootSaga
