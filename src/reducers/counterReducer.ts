import { ICounterAction } from '../actions/counterActions'
import Constants from '../constants'

const counterReducer: Redux.Reducer<number> = (
  state: number = 0,
  action: ICounterAction
): number => {

  switch (action.type) {

    case Constants.ActionType.Counter.INCREMENT_COUNTER:
      return state + (action.amount || 1)

    case Constants.ActionType.Counter.DECREMENT_COUNTER:
      return state - (action.amount || 1)

    default:
      return state
  }

}

export default counterReducer
