import {
  addTodoAction,
  deleteAllTodosAction,
  deleteTodoAction,
  toggleTodoStatusAction,
  setTodoFilterAction
} from '../actions/todosActions'
import AddTodo from '../components/AddTodo'
import TodoList from '../components/TodoList'
import { connect } from 'react-redux'

export interface ITodosRouteProps {
  todos: ITodoList
  filter: ITodoFilter
  addTodo: (text: string) => void
  toggleTodoStatus: (id: number) => void
  deleteTodo: (id: number) => void
  deleteAllTodos: () => void
  setFilter: (filter: ITodoFilter) => void
}

const TodosRoute = (props: Readonly<ITodosRouteProps>) => {

  const { todos, filter, addTodo, toggleTodoStatus, deleteTodo, deleteAllTodos, setFilter } = props

  const setFilterHandler = (f: ITodoFilter): MouseEventHandler => (event) => setFilter(f)
  const deleteAllTodosHandler = (): MouseEventHandler => (event) => deleteAllTodos()

  const filtered = filter === 'Active'
    ? todos.filter(todo => !todo.completed)
    : filter === 'Completed'
      ? todos.filter(todo => todo.completed)
      : todos

  const badge = filtered.size ? filtered.size.toString() : ''

  return (
    <div className="container">
      <div className="panel panel-default">
        <div className="panel-heading">
          <h1 className="panel-title">To Do List<span className="badge pull-right">{badge}</span></h1>
        </div>
        <div className="panel-body">
          <AddTodo addTodo={addTodo} />
          <TodoList todos={filtered} toggleTodoStatus={toggleTodoStatus} deleteTodo={deleteTodo} />
          <div className="btn-group">
            <button
              type="button"
              className={`btn btn-default ${filter === 'All' ? ' active' : ''}`}
              onClick={setFilterHandler('All')}
            >
              All
            </button>
            <button
              type="button"
              className={`btn btn-default ${filter === 'Active' ? ' active' : ''}`}
              onClick={setFilterHandler('Active')}
            >
              Active
            </button>
            <button
              type="button"
              className={`btn btn-default ${filter === 'Completed' ? ' active' : ''}`}
              onClick={setFilterHandler('Completed')}
            >
              Done
            </button>
          </div>
          <button
            type="button"
            disabled={todos.size === 0}
            className="btn btn-danger pull-right"
            onClick={deleteAllTodosHandler()}
          >
            Delete All
          </button>
        </div>
      </div>
    </div>
  )

}

const mapStateToProps = (state: Readonly<IAppState>) => {
  const { todos, filter } = state
  return {
    todos,
    filter
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<IAppState>) => {
  return {
    addTodo: (text: string) => { dispatch(addTodoAction(text)) },
    toggleTodoStatus: (id: number) => { dispatch(toggleTodoStatusAction(id)) },
    deleteTodo: (id: number) => { dispatch(deleteTodoAction(id)) },
    deleteAllTodos: () => { dispatch(deleteAllTodosAction()) },
    setFilter: (filter: ITodoFilter) => { dispatch(setTodoFilterAction(filter)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosRoute)
