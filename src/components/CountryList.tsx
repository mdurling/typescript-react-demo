export interface ICountryListProps {
  countries: ICountryList,
  selectedCountry: ICountry,
  selectedCountryChange: (selected: ICountry) => void
}

const CountryList = (props: Readonly<ICountryListProps>) => {

  let controls: {
    select?: HTMLSelectElement
  } = {}

  const { countries, selectedCountry, selectedCountryChange } = props

  const onChangeHandler: FormEventHandler = (event) => {
    selectedCountryChange(countries.find(country => country.alpha2Code === controls.select.value))
  }

  const selectRef = (select: HTMLSelectElement) => { controls.select = select }

  if (!countries.size) {
    return <span><i className="fa fa-spinner fa-spin" /> Loading Countries...</span>
  }

  return (
    <select ref={selectRef} className="form-control" onChange={onChangeHandler}>
      <option key="" value="" selected={selectedCountry === null}>Select a Country</option>
      {
        countries.map(country =>
          (
            <option key={country.alpha2Code} value={country.alpha2Code} selected={country === selectedCountry}>
              {country.name}{country.name === country.nativeName ? '' : ` (${country.nativeName})`}
            </option>
          )
        )
      }
    </select>
  )
}

export default CountryList
