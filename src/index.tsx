import Counter from './routes/Counter'
import Countries from './routes/Countries'
import Home from './routes/Home'
import Layout from './routes/Layout'
import Todos from './routes/Todos'
import store from './store'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import {
  Route,
  Router,
  browserHistory,
  // hashHistory,
  IndexRoute
} from 'react-router'

// Use require so Webpack will load css/favicon
require('./site.css')
require('../images/favicon.ico?output=images/favicon.ico')

// Render the React application to the DOM
render(
  (
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="/" component={Layout}>
          <IndexRoute component={Home} />
          <Route path="countries" component={Countries} />
          <Route path="counter" component={Counter} />
          <Route path="todos" component={Todos} />
        </Route>
      </Router>
    </Provider>
  ),
  document.getElementById('reactjs')
)
