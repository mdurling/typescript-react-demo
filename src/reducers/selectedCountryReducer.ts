import { ISelectedCountryChangeAction } from '../actions/countriesActions'
import Constants from '../constants'

const selectedCountryReducer: Redux.Reducer<ICountry> = (
  state: ICountry = null,
  action: Redux.Action
): ICountry => {

  switch (action.type) {

    case Constants.ActionType.Countries.SELECTED_COUNTRY_CHANGE:
      return (action as ISelectedCountryChangeAction).selected

    default:
      return state

  }
}

export default selectedCountryReducer
