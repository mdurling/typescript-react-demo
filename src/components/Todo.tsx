export interface ITodoProps {
  todo: ITodo
  toggleTodoStatus: (id: number) => void
  deleteTodo: (id: number) => void
}

const Todo = (props: Readonly<ITodoProps>) => {

  const { todo, toggleTodoStatus, deleteTodo } = props
  const toggleTodoStatusHandler = (): MouseEventHandler => (event) => {
    toggleTodoStatus(todo.id)
  }
  const deleteTodoHandler = (): MouseEventHandler => (event) => {
    event.stopPropagation()
    deleteTodo(todo.id)
  }

  return (
    <div
      role="button"
      title={todo.completed ? 'Mark Active' : 'Mark Completed'}
      className={todo.completed
        ? 'list-group-item list-group-item-danger'
        : 'list-group-item list-group-item-success'
      }
      style={todo.completed ? { textDecoration: 'line-through' } : {}}
      onClick={toggleTodoStatusHandler()}
    >
      <span>{todo.text}</span>
      <div className="close" title="Delete Item" onClick={deleteTodoHandler()}>&times; </div>
    </div>
  )

}

export default Todo
