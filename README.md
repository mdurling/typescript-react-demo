# React/Redux Demo using TypeScript/Babel
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)
[ ![Codeship Status for mdurling/typescript-react-demo](https://codeship.com/projects/d309bc30-4de8-0134-bbf7-468259afc5dd/status?branch=master)](https://codeship.com/projects/170673)

## Background
This project was created for the sole purpose of educating myself on the concepts of
[React](https://facebook.github.io/react/),
[React-Router](https://github.com/reactjs/react-router),
[Redux](https://github.com/reactjs/redux), and
[Immutable](https://facebook.github.io/immutable-js/) data/state
while building a simple application using
[TypeScript](https://www.typescriptlang.org/),
[Babel](https://babeljs.io/), and
[Webpack](https://webpack.github.io/).
I hope others can benefit from my learning experience.

## A Note on TypeScript/Babel and ES2015 (ES6) vs. ES5
Several modern evergreen browsers (most notably, Chrome) already support ES6; many others (for example, Safari), however, do not.
While the TypeScript compiler can target either ES6 or ES5 compliant JavaScript; some ES6 features such as promises and generators
are not supported when targeting ES5. In order to generate ES5 compatible JavaScript supported by most browsers, use the latest
ES6 features, and still retain the benefit of TypeScript's type checking this project has been configured to first compile
TypeScript to ES6 and then use Babel's transpiler and runtime polyfill to generate ES5 compliant JavaScript.

`TS/TSX -> (TypeScript) -> ES6 JS/JSX -> (Babel) -> ES5 JS`

# Usage

#### `npm install`
Install Node modules listed in `package.json`

#### `npm run inline`
Runs the webpack dev server (WDS) with Hot Module Replacement (HMR). The application can be found at `http://localhost:8080`

#### `npm run inline -- --host <ip>`
Runs WDS with HMR. The application can be found at `http://<ip>:8080`

#[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

The use of [JavaScript Standard Style](http://standardjs.com/) is encouraged in this project
which uses [TSLint](http://palantir.github.io/tslint/) to check all TypeScript code (.ts and .tsx files) for readability, maintainability, functionality errors, and code style.

You can run TSLint manually from the command line with either `npm run tslint` or
`npm run deploy` (the latter runs the Webpack build process if there are no linter errors or warnings). Also, if you are using
Visual Studio Code as your editor/IDE, there is an excellent TSLint
[Extension](https://marketplace.visualstudio.com/items?itemName=eg2.tslint) that you should install to get automatic linting while editing.

### TSLint Rules

[StandardJS Rules](https://www.npmjs.com/package/tslint-config-standard)

[React Rules](https://www.npmjs.com/package/tslint-react)

# Live Demo
There is a live demo of this project available [here](http://typescript-react-demo.michaeldurling.com.s3-website-us-east-1.amazonaws.com/)