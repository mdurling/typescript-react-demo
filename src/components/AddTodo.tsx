export interface IAddTodoProps {
  addTodo: (text: string) => void
}

const AddTodo = (props: Readonly<IAddTodoProps>) => {

  let controls: {
    textInput?: HTMLInputElement
  } = {}

  const { addTodo } = props

  const addTodoHandler: MouseEventHandler = (event) => {
    event.preventDefault()
    let text = controls.textInput.value.trim()
    if (text) {
      addTodo(text)
      controls.textInput.value = ''
      controls.textInput.focus()
    }
  }

  const textInputRef = (ref: HTMLInputElement): void => {
    controls.textInput = ref
  }

  return (
    <form onSubmit={addTodoHandler}>
      <div className="form-group">
        <span className="input-group">
          <input type="text" placeholder="Enter New Item" className="form-control" ref={textInputRef} />
          <span className="input-group-btn">
            <button className="btn btn-primary" type="button" title="Add Task" onClick={addTodoHandler}>
              Add Item
            </button>
          </span>
        </span>
      </div>
    </form>
  )

}

export default AddTodo
