import * as axios from 'axios'

export function* getAllCountries () {
  let xhr: Axios.AxiosXHR<ICountry[]> = yield axios.get('https://restcountries.eu/rest/v1')
  return xhr.data
}

export default {
  getAllCountries
}
