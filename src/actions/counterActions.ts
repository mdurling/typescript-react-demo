import Constants from '../constants'
import * as Redux from 'redux'

export interface ICounterAction extends Redux.Action {
  amount?: number
}

export const incrementCounterAction = (amount?: number): ICounterAction => {
  return {
    type: Constants.ActionType.Counter.INCREMENT_COUNTER,
    amount
  }
}

export const decrementCounterAction = (amount?: number): ICounterAction => {
  return {
    type: Constants.ActionType.Counter.DECREMENT_COUNTER,
    amount
  }
}
