export interface ICounterProps {
  count: number
  increment: (amount?: number) => void
  decrement: (amount?: number) => void
}

const Counter = (props: Readonly<ICounterProps>) => {

  const { count, increment, decrement } = props
  const incrementHandler = (amount?: number): MouseEventHandler => ((event) => increment(amount))
  const decrementHandler = (amount?: number): MouseEventHandler => ((event) => decrement(amount))

  return (
    <div style={{ margin: '10px' }}>
      <button type="button" className="btn btn-success" onClick={incrementHandler()}>+</button>
      <span style={{ padding: '0 5px' }}>{count}</span>
      <button type="button" className="btn btn-danger" onClick={decrementHandler()}>-</button>
    </div>
  )

}

export default Counter
