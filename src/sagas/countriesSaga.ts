import {
  getAllCountriesResultAction,
  getAllCountriesErrorAction
} from '../actions/countriesActions'
import constants from '../constants'
import { Saga } from 'redux-saga'
import { takeEvery } from 'redux-saga'
import { call, fork, put} from 'redux-saga/effects'
import restcountries from '../services/restcountries'
import { Effect } from 'redux-saga/effects'

const countriesSaga: Saga = function* (): Iterable<Effect> {
  yield fork(
    takeEvery,
    constants.ActionType.Countries.GET_ALL_COUNTRIES_REQUEST,
    function* (): Iterable<Effect> {
      try {
        let data: ICountry[] = yield call(restcountries.getAllCountries)
        yield put(getAllCountriesResultAction(data))
      } catch (error) {
        yield put(getAllCountriesErrorAction(error))
      }
    }
  )
}

export default countriesSaga
