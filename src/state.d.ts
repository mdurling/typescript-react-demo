// Todo Item
interface ITodo {
  id: number
  text: string
  completed: boolean
}

// Todo List Filter Values
type ITodoFilter = 'All' | 'Active' | 'Completed'

// Country
interface ICountry {
  alpha2Code: string
  alpha3Code: string
  altSpellings: string[]
  area: number
  borders: string[]
  callingCodes: string[]
  capital: string
  currencies: string[]
  demonym: string
  gini: number
  languages: string[]
  latlng: number[]
  name: string
  nativeName: string
  population: number
  region: string
  relevance: string
  subregion: string
  timezones: string[]
  topLevelDomain: string[]
  translations: {}
}

// Create some aliases for frequently used types
type ICountryList = Immutable.List<ICountry>
type ITodoList = Immutable.List<ITodo>
type FormEventHandler = __React.EventHandler<__React.FormEvent>
type MouseEventHandler = __React.EventHandler<__React.MouseEvent>

// Application State generally there is a separate reducer for each property
interface IAppState {
  counter: number
  todos: ITodoList
  filter: ITodoFilter
  countries: ICountryList
  selectedCountry: ICountry
}
