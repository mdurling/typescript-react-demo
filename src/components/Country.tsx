import * as numeral from 'numeral'

export interface IPropertyProps {
  label: string
}

export interface IStringPropertyProps extends IPropertyProps {
  value: string
}

const StringProperty = (props: Readonly<IStringPropertyProps>) => {
  const { label, value } = props
  return (
    <div className="control-group row">
      <label className="control-label col-xs-3">{label}</label>
      <div className="controls col-xs-9">
        <p className="form-control-static" dangerouslySetInnerHTML={{'__html': value || 'None'}} />
      </div>
    </div>
  )
}

export interface IStringArrayPropertyProps extends IPropertyProps {
  values: string[]
}

const StringArrayProperty = (props: Readonly<IStringArrayPropertyProps>) => {
  const { label, values } = props
  return (
    <StringProperty
      label={label}
      value={(values ? values.join(', ') : '') || 'None'}
    />
  )
}

export interface INumberPropertyProps extends IPropertyProps {
  value: number
}

const NumberProperty = (props: Readonly<INumberPropertyProps>) => {
  const { label, value } = props
  return (
    <StringProperty
      label={label}
      value={numeral(value).format('0,0')}
    />
  )
}

export interface ILatLngPropertyProps extends IPropertyProps {
  lat: number,
  lng: number
}

const LatLngProperty = (props: Readonly<ILatLngPropertyProps>) => {
  const { label, lat, lng } = props
  return (
    <StringProperty
      label={label}
      value={`${Math.abs(lat)}&deg;${lat < 0 ? 'S' : 'N'}, ${Math.abs(lng)}&deg;${lng < 0 ? 'W' : 'E'}`}
    />
  )
}

export interface ICountryProps {
  country: ICountry
}

const Country = (props: Readonly<ICountryProps>) => {

  const { country } = props

  if (country) {
    return (
      <form className="form-horizontal" style={{ marginTop: 15 }}>
        <StringProperty label="Alpha 2 Code" value={country.alpha2Code} />
        <StringProperty label="Alpha 3 Code" value={country.alpha3Code} />
        <NumberProperty label="Area" value={country.area} />
        <StringArrayProperty label="Alternate Spellings" values={country.altSpellings} />
        <StringArrayProperty label="Bordering Countries" values={country.borders} />
        <StringArrayProperty label="Calling Codes" values={country.callingCodes} />
        <StringProperty label="Capital" value={country.capital} />
        <StringArrayProperty label="Currencies" values={country.currencies} />
        <StringProperty label="Demonym" value={country.demonym} />
        <StringArrayProperty label="Languages" values={country.languages} />
        <LatLngProperty label="Lat/Lng" lat={country.latlng[0]} lng={country.latlng[1]} />
        <StringProperty label="Native Name" value={country.nativeName} />
        <NumberProperty label="Population" value={country.population} />
        <StringProperty label="Region" value={country.region} />
        <StringProperty label="Subregion" value={country.subregion} />
        <StringArrayProperty label="Timezones" values={country.timezones} />
        <StringArrayProperty label="TLD" values={country.topLevelDomain} />
      </form>
    )

  }

  return null

}

export default Country
