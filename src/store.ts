import {
  applyMiddleware,
  combineReducers,
  createStore,
  compose
} from 'redux'

// Reducers
import counter from './reducers/counterReducer'
import countries from './reducers/countriesReducer'
import selectedCountry from './reducers/selectedCountryReducer'
import filter from './reducers/filterReducer'
import todos from './reducers/todosReducer'

const reducers = combineReducers({
  counter,
  todos,
  filter,
  countries,
  selectedCountry
})

// Logger Middleware
import * as createLoggerMiddleware from 'redux-logger'
const loggerMiddleware = createLoggerMiddleware({ collapsed: true })

// Saga Middleware
import createSagaMiddleware from 'redux-saga'
const sagaMiddleware = createSagaMiddleware()

const middleware = applyMiddleware(loggerMiddleware, sagaMiddleware)

// Redux DevTools Extension
const ReduxDevTools = (window as any).devToolsExtension ? (window as any).devToolsExtension() : undefined

// Create Store
export default ReduxDevTools
  ? createStore(reducers, compose(middleware, ReduxDevTools))
  : createStore(reducers, middleware)

// Run Sagas
import rootSaga from './sagas'
sagaMiddleware.run(rootSaga)
