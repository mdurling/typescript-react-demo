import { getAllCountriesRequestAction, selectedCountryChangeAction } from '../actions/countriesActions'
import CountryList from '../components/CountryList'
import Country from '../components/Country'
import * as React from 'react'
import { connect } from 'react-redux'

export interface ICountriesProps {
  countries: ICountryList
  selectedCountry: ICountry
  getAllCountries (): void
  selectedCountryChange (selected: ICountry): void
}

class CountriesRoute extends React.Component<Readonly<ICountriesProps>, void> {
  public componentWillMount () {
    const { getAllCountries, countries } = this.props
    if (countries.size === 0) {
      getAllCountries()
    }
  }
  public render () {
    const { countries, selectedCountry, selectedCountryChange } = this.props
    return (
      <div>
        <h1>Countries</h1>
        <CountryList
          countries={countries}
          selectedCountry={selectedCountry}
          selectedCountryChange={selectedCountryChange}
        />
        <Country country={selectedCountry} />
      </div>
    )
  }
}

const mapStateToProps = (state: Readonly<IAppState>) => {
  const { countries, selectedCountry } = state
  return {
    countries,
    selectedCountry
  }
}

const mapDispatchToProps = (dispatch: Redux.Dispatch<IAppState>) => {
  return {
    getAllCountries: () => { dispatch(getAllCountriesRequestAction()) },
    selectedCountryChange: (selected: ICountry) => { dispatch(selectedCountryChangeAction(selected)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CountriesRoute)
