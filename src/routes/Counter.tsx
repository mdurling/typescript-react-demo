import {
  decrementCounterAction,
  incrementCounterAction
} from '../actions/counterActions'
import Counter from '../components/Counter'
import { connect } from 'react-redux'

export interface ICounterRouteProps {
  value: number
  incrementCounter: (amount?: number) => void
  decrementCounter: (amount?: number) => void
}

const CounterRoute = (props: Readonly<ICounterRouteProps>) => {
  const { value, incrementCounter, decrementCounter } = props
  return (
    <div>
      <h1>Counter</h1>
      <Counter count={value} increment={incrementCounter} decrement={decrementCounter} />
    </div>
  )
}

function mapStateToProps (state: IAppState) {
  const { counter } = state
  return {
    value: counter
  }
}

function mapDispatchToProps (dispatch: Redux.Dispatch<IAppState>) {
  return {
    incrementCounter: (amount?: number) => {
      dispatch(incrementCounterAction(amount))
    },
    decrementCounter: (amount?: number) => {
      dispatch(decrementCounterAction(amount))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterRoute)
