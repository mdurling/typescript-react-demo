import Constants from '../constants'
import * as Redux from 'redux'

export interface IGetAllCountriesRequestAction extends Redux.Action {
}

export const getAllCountriesRequestAction = (): IGetAllCountriesRequestAction => {
  return {
    type: Constants.ActionType.Countries.GET_ALL_COUNTRIES_REQUEST
  }
}

export interface IGetAllCountriesResultAction extends Redux.Action {
  countries: ICountry[]
}

export const getAllCountriesResultAction = (countries: ICountry[]): IGetAllCountriesResultAction => {
  return {
    type: Constants.ActionType.Countries.GET_ALL_COUNTRIES_RESULT,
    countries
  }
}

export interface IGetAllCountriesErrorAction extends Redux.Action {
  error: Error
}

export const getAllCountriesErrorAction = (error: Error): IGetAllCountriesErrorAction => {
  return {
    type: Constants.ActionType.Countries.GET_ALL_COUNTRIES_ERROR,
    error
  }
}

export interface ISelectedCountryChangeAction extends Redux.Action {
  selected: ICountry
}

export const selectedCountryChangeAction = (selected: ICountry): ISelectedCountryChangeAction => {
  return {
    type: Constants.ActionType.Countries.SELECTED_COUNTRY_CHANGE,
    selected
  }
}
