import * as React from 'react'
import { IndexLinkContainer, LinkContainer } from 'react-router-bootstrap'
import { RouterOnContext, withRouter } from 'react-router'

const avatar = require('../../images/avatar.jpg')

export interface ILayoutProps {
  router?: RouterOnContext
  children?: React.ReactNode
}

// Application layout
export class Layout extends React.Component<Readonly<ILayoutProps>, void> {

  public render () {

    const { children, router } = this.props
    const indexOnly: boolean = true

    return (
      <div className="container-fluid">
        <nav className="navbar navbar-inverse navbar-fixed-top">
          <div className="container-fluid">
            <div className="navbar-header">
              <span className="navbar-brand">
                <a
                  href="http://www.michaeldurling.com"
                  title="M/CHAEL DURL/NG"
                  target="external"
                  role="button"
                >
                  <img
                    src={avatar}
                    style={{ width: 51, height: 51, marginTop: -15, marginRight: 5, display: 'inline-block' }}
                  />
                  <span style={{ verticalAlign: 'top' }}>M/CHAEL DURL/NG</span>
                </a>
              </span>
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#links">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
            </div>
            <div className="navbar-collapse collapse" id="links">
              <ul className="nav navbar-nav">
                <li
                  className={router.isActive({ pathname: '/' }, indexOnly) ? 'active' : ''}
                  data-toggle="collapse"
                  data-target=".navbar-collapse"
                >
                  <IndexLinkContainer to="/"><a href="" role="button">Home</a></IndexLinkContainer>
                </li>
                <li
                  className={router.isActive({ pathname: '/counter' }) ? 'active' : ''}
                  data-toggle="collapse"
                  data-target=".navbar-collapse"
                >
                  <LinkContainer to="/counter"><a href="" role="button">Counter</a></LinkContainer>
                </li>
                <li
                  className={router.isActive({ pathname: '/countries' }) ? 'active' : ''}
                  data-toggle="collapse"
                  data-target=".navbar-collapse"
                >
                  <LinkContainer to="/countries"><a href="" role="button">Countries</a></LinkContainer>
                </li>
                <li
                  className={router.isActive({ pathname: '/todos' }) ? 'active' : ''}
                  data-toggle="collapse"
                  data-target=".navbar-collapse"
                >
                  <LinkContainer to="/todos"><a href="" role="button">Todos</a></LinkContainer>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div style={{ marginTop: '70px' }}>
          {children}
        </div>
      </div>
    )

  }

}

// Make the router available to the component as a property
export default withRouter(Layout)
