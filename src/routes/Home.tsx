export interface IHomeProps { }

const Home = (props: Readonly<IHomeProps>) => (
  <div className="home">
    <div className="container">
      <div className="jumbotron">
        <h1>TypeScript React/Redux Demo</h1>
        <p>
          <span>This project was created for the sole purpose of educating myself on the concepts of </span>
          <span><a href="https://facebook.github.io/react/" target="external">React</a>, </span>
          <span><a href="https://github.com/reactjs/react-router" target="external">React-Router</a>, </span>
          <span><a href="https://github.com/reactjs/redux" target="external">Redux</a>, and </span>
          <span><a href="https://facebook.github.io/immutable-js/" target="external">Immutable</a> data/state </span>
          <span>while building a basic application using </span>
          <span><a href="https://www.typescriptlang.org/" target="external">TypeScript</a>, </span>
          <span><a href="https://babeljs.io/" target="external">Babel</a> and </span>
          <span><a href="https://webpack.github.io/" target="external">Webpack</a>. </span>
        </p>
        <p>
          <span>View source code on </span>
          <a href="https://bitbucket.org/mdurling/typescript-react-demo/src" target="external">Bitbucket</a>
        </p>
      </div>
    </div>
  </div>
)

export default Home
