var WebpackNotifierPlugin = require('webpack-notifier');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var WriteFileWebpackPlugin = require('write-file-webpack-plugin');
var version = require('./package.json').version;
var path = require('path');
var validate = require('webpack-validator');

var PATHS = {
  src: path.join(__dirname, 'src/'),
  images: path.join(__dirname, 'images/'),
  build: path.join(__dirname, 'build/')
};

module.exports = validate(
  {
    entry: PATHS.src,
    output: {
      path: PATHS.build,
      filename: '[name]-' + version + '-[hash].js'
    },

/*
    devServer: {
        outputPath: PATHS.build
    },
*/

    // Enable sourcemaps for debugging webpack's output.
    devtool: 'source-map',

    resolve: {
      // Add '.ts' and '.tsx' as resolvable extensions.
      extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.jsx']
    },

    module: {
      loaders: [
        /*
            TypeScript can emit either ES6 or ES5 compliant JavaScript, however, the TypeScript compiler does not support
            promises, generators, and certain other ES6 features when targeting ES5. Therefore, first compile the TypeScript
            code (all files with a '.ts' or '.tsx' extension) to ES6 (ts-loader) and then transpile it to ES5 using
            Babel (babel-loader).

            TS/TSX -> (TypeScript) -> ES6 JS/JSX -> (Babel) -> ES5 JS

        */
        { test: /\.tsx?$/, loaders: ['babel', 'ts'], include: PATHS.src },
        { test: /\.css$/, loader: ExtractTextWebpackPlugin.extract('style', 'css?minimize'), include: PATHS.src },
        { test: /\.(jpg|png|gif)$/, loader: 'url?limit=2048&name=[path][name].[hash].[ext]', include: PATHS.images },
        { test: /\.(ico)$/, loader: 'static', include: PATHS.images }
      ],

      preLoaders: [
        // All output '.js' files will have any sourcemaps re-processed by source-map-loader.
        { test: /\.js$/, loader: 'source-map', include: PATHS.src }
      ]
    },

    plugins: [

      // Set up the notifier plugin - you can remove this (or set alwaysNotify false) if desired
      new WebpackNotifierPlugin({ alwaysNotify: true }),

      // Set up the html template plugin
      new HtmlWebpackPlugin({
        template: __dirname + '/src/index.html',
        filename: 'index.html',
        inject: 'body'
      }),

      new ExtractTextWebpackPlugin('[name]-' + version + '-[hash].css')

     // new WriteFileWebpackPlugin()

    ],

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
      'react': 'React',
      'react-dom': 'ReactDOM',
      'react-redux': 'ReactRedux',
      'redux': 'Redux',
      'react-router': 'ReactRouter',
      'react-router-bootstrap': 'ReactRouterBootstrap',
      'redux-logger': 'reduxLogger',
      'redux-saga': 'ReduxSaga',
      'redux-saga/effects': 'ReduxSaga.effects',
      'axios': 'axios',
      'numeral': 'numeral',
      'immutable': 'Immutable'
    },
  }
);
